//
//  HolofyDemoTests.swift
//  HolofyDemoTests
//
//  Created by Ashish on 26/06/21.
//

import XCTest
@testable import HolofyDemo

class HolofyDemoTests: XCTestCase {

    var viewModel:ViewModelProtocol!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testDate() throws {
        if let string = getStringDateFromDate(date: Date(), requiredDateFormate: .longDate) , string.count > 0 {

        } else {
            XCTFail("Unable to parse date")
        }
    }
    
    func testJSONMapping() throws {
        viewModel = TestViewModel()
        viewModel.apiHit { (errorMessage) in
            if let errorMessage = errorMessage {
                XCTFail(errorMessage)
            }
        }
    }

}

// MARK: - Testing Extension

class TestViewModel:ViewModelProtocol {
    
    func apiHit(handler:@escaping (_ success:String?)->Void) {
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: "Demo", withExtension: "json") else {
           handler(nil)
            return
        }
        do {
            let json = try Data(contentsOf: url)
            let parseJson = try JSONSerialization.jsonObject(with: json, options: JSONSerialization.ReadingOptions.allowFragments)
            if let data = parseJson as? [String : Any] {
                let model = Weather.saveData(data: data)
                if model.currentWeather > 0 {
                    if model.minTemp > 0 {
                        if model.weatherDesc?.isEmpty == false {
                            if model.maxTemp > 0 {
                                handler(nil)
                            } else {
                                handler("maxTemp weather is nil")
                            }
                        } else {
                            handler("weather Desc is nil")
                        }
                    } else {
                        handler("minTemp weather is nil")
                    }
                } else {
                    handler("currentWeather weather is nil")
                }
                
            } else {
                handler(NetworkError.noData.errorMessage())
            }
        }catch {
            handler(NetworkError.unableToDecode.errorMessage())
        }
    }
}


