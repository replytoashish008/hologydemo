//
//  Marker.swift
//  HolofyDemo
//
//  Created by Ashish on 28/06/21.
//

import Foundation
import CoreData
import MapKit

extension Markers : ReuseableProtocol {
    
    struct Keys {
        static let address = "address"
        static let isFav = "isFav"
    }
     
    static func saveData(data: MKPlacemark) -> Markers {
        let address = getAddressFrom(data: data)
        let predicate:NSPredicate? = address.count > 0 ? NSPredicate.init(format: "\(Keys.address) == %@", address)  : nil
        if let value = getFetchRecord(predicate: predicate, classObject: Markers.reusableIdentifier).first as? Markers {
            
            return value
        } else {
            let value = NSEntityDescription.insertNewObject(forEntityName: Markers.reusableIdentifier, into: context) as! Markers
            value.updateData(data: data)
            return value
        }
    }
    
    private static func getAddressFrom(data: MKPlacemark) -> String {
        var address = ""
        if let ocean = data.ocean, ocean.trimString.count > 0 {
            address = ocean
        } else if let inlandWater = data.inlandWater , inlandWater.trimString.count > 0 {
            address = inlandWater
        } else {
            if let city = data.locality, city.trimString.count > 0 {
                address = city
            }
            if let state = data.administrativeArea , state.trimString.count > 0 {
                address = address.count > 0 ? address + ", " + state : state
            }
            if let country = data.country , country.trimString.count > 0 {
                address = address.count > 0 ? address + ", " + country : country
            }
        }
        return address
    }

    func updateData(data: MKPlacemark)  {
        self.inlandWater = data.inlandWater
        self.ocean = data.ocean
        
        self.city = data.locality
        self.state = data.administrativeArea
        self.country = data.country
        
        self.address = Markers.getAddressFrom(data: data)
        
        self.lat = data.coordinate.latitude
        self.longi = data.coordinate.longitude
    }
    
    func saveData(weather: [String : Any]?) {
        if let weather = weather {
            let weather = Weather.saveData(data: weather)
            weather.hasMarkers = self
            self.hasWeather = weather
        }
    }

}

