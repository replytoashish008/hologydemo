//
//  Location.swift
//  HolofyDemo
//
//  Created by Ashish on 02/07/21.
//

import Foundation
import CoreData
import MapKit

extension Location : ReuseableProtocol   {
    
    struct Keys {
        static let title = "title"
        static let isFav = "isFav"
    }
        
    static func saveData(title: String) -> Location {
        var location :Location!
        if let value = getFetchRecord(predicate: NSPredicate.init(format: "\(Keys.title) == %@", title), classObject: Location.reusableIdentifier).first as? Location {
            value.lastAccess = Date()
            location = value
        } else {
            let value = NSEntityDescription.insertNewObject(forEntityName: Location.reusableIdentifier, into: context) as! Location
            value.title = title
            value.lastAccess = Date()
            location =  value
        }
        saveCoreData()
        return location
    }
    
    static func getAllFav() -> [Location] {
        if let value = getFetchRecord(predicate: NSPredicate(format: "\(Keys.isFav) == %d", true), classObject: Location.reusableIdentifier) as? [Location] {
            return value
        } else {
            return []
        }
    }


    func updateDataMarker(data: MKPlacemark)  {
        let marker = Markers.saveData(data: data)
        marker.addToHasLocation(self)
        self.hasMarker = marker
        saveCoreData()
    }
    
    func updateDataFav(isFav: Bool)  {
        self.isFav = isFav
        saveCoreData()
    }
    
    func updateDataWithWeather(data: [String : Any]?)  {
        if let data = data {
            if let weather = hasMarker?.hasWeather {
                weather.updateData(data: data)
            } else {
                let weather = Weather.saveData(data: data)
                weather.hasMarkers = self.hasMarker
                self.hasMarker?.hasWeather = weather
            }
            saveCoreData()
        }
    }
}

