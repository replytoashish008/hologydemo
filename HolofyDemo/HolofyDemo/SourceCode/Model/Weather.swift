//
//  Weather.swift
//  HolofyDemo
//
//  Created by Ashish on 26/06/21.
//

import Foundation
import CoreData

extension Weather : ReuseableProtocol   {
    
    struct keys {
        static let sys = "sys"
        static let main = "main"
        static let temp = "temp"
        static let temp_max = "temp_max"
        static let temp_min = "temp_min"
        static let weather = "weather"
        static let description = "description"
    }
    
    static func saveData(data: [String : Any]) -> Weather {
        var weatherObj:Weather!
        weatherObj = NSEntityDescription.insertNewObject(forEntityName: Weather.reusableIdentifier, into: context) as? Weather
        weatherObj.updateData(data: data)
        return weatherObj
    }


    func updateData(data: [String : Any])  {
        self.date = Date()
        if let main = data[keys.main] as? [String:Any] {
            if let value = main[keys.temp] as? Double {
                self.currentWeather = value
            }
            if let value = main[keys.temp_min] as? Double {
                self.minTemp = value
            }
            if let value = main[keys.temp_max] as? Double {
                self.maxTemp = value
            }
            
        }
        
        if let weather = data[keys.weather] as? [[String:Any]] {
            var desc = ""
            for value in weather {
                if let value = value[keys.description] as? String {
                    if desc.count == 0 {
                        desc = value
                    } else {
                        desc = desc + ", " + value
                    }
                }
            }
            self.weatherDesc = desc
        }
    }
}

