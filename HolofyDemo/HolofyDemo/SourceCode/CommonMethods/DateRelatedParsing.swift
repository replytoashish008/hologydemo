//
//  File.swift
//  HolofyDemo
//
//  Created by Ashish on 02/07/21.
//

import Foundation

//MARK:-  Date Related Parsing
func getDateFromString(dateString:String?, withDateFormate:DateFormate) throws -> Date? {
    
    if let dateString = dateString {
        
        let dateFormate = DateFormatter()
        dateFormate.dateFormat = withDateFormate.rawValue
        // error is occour when taking di
        // let locale = NSLocale.init(localeIdentifier: "\(NSTimeZone.defaultTimeZone())")
        // dateFormate.locale = locale
        
        dateFormate.timeZone = NSTimeZone.local
        let date = dateFormate.date(from: dateString)
        return date
    }
    return nil
}

func getStringDateFromDate(date:Date? , requiredDateFormate:DateFormate) -> String? {
    if let date = date {
        let userDateFormate = DateFormatter()
        userDateFormate.dateFormat = requiredDateFormate.rawValue
        return userDateFormate.string(from: date)
    }
    return nil
}
func getStringDateFromString(dateString:String? , requiredDateFormate:DateFormate , withDateFormate:DateFormate) -> String? {
    
    if let dateString = dateString {
        do {
            if let date  = try getDateFromString(dateString: dateString, withDateFormate: withDateFormate) {
                return getStringDateFromDate(date: date, requiredDateFormate: requiredDateFormate)
            }
        } catch {
            print(error)
            return nil
        }
    }
    return nil
}

enum DateFormate:String {
    case fullServerDateFormate = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.zzz'Z'"
    case fullDateFormate = "yyyy'-'MM'-'dd'T'HH':'mm':'ss"
    case longDate = "yyyy'-'MM'-'dd HH':'mm"
    case numberDate = "dd-MM-yyyy"
    case hourMins = "HH':'mm'"
}
