//
//  NetworkProtocol.swift
//  HolofyDemo
//
//  Created by Ashish on 02/07/21.
//

import Foundation

typealias Parameters = [String:Any]
typealias handler = (_ response:Any? , _ error:Error?,_ tag:String? , _ statusCode:Int?)->Void

// MARK:- ParameterEncoder
protocol ParameterEncoder {
    static func encode(parameters: Parameters?) throws -> Data?
}


// MARK:- URL String From Paramters
protocol StringFromHttpParamters {
    static func stringFromHttpParameters(data:Parameters?) -> String?
}

// MARK:- NetworkUrl
protocol NetworkUrl {
    var methodType:APIHttpMethod {get}
    var extendedUrl:String {get}
    var apiContentType: APIContentType {get}
}

// MARK:- NetworkRouter
protocol NetworkRouter: AnyObject {
    // , arrPar arrBodyParameter:[Any]?
    func data_request(bodyencodedPata:[String:Any]?, urlEncodingPara:[String:Any]?, handler:@escaping handler )
    func cancelTask()
    func isRunning() -> Bool
}

// MARK:- NetworkErrorProtocol
protocol NetworkErrorProtocol {
    func errorMessage() -> String?
}

// MARK:- API_httpMethod
enum APIHttpMethod {
    
    case get
    case delete
    case post
    case put
    case patch(body: Data)
    
    var rawString: String {
        switch self {
        case .get:
            return "GET"
        case .delete:
            return "DELETE"
        case .post:
            return "POST"
        case .put://(_):
            return "PUT"
        case .patch(_):
            return "PATCH"
        }
    }
}

// MARK:- APIContentType
enum APIContentType: String {
    case json = "application/json"
    case x_www_form_urlencoded = "application/x-www-form-urlencoded"
    case formData = "multipart/form-data"
    case none = ""
}



enum Result{
    case success
    case failure(Error)
}

