//
//  NetworkOperations.swift
//  HolofyDemo
//
//  Created by Ashish on 02/07/21.
//

import Foundation

import UIKit

class NetworkOperations<T:NetworkUrl>: Operation , NetworkRouter {

    // MARK:- variable
    var baseUrl = "http://api.openweathermap.org/data/2.5/"

    // MARK:- variable
    private var tag:T
    private var handlers:handler?
    private var task:URLSessionDataTask?
    // MARK:- init
    init(tag:T) {
        self.tag = tag
    }

    private var requestNotCorrectCount = 0

    deinit {
        print("deinit " + self.tag.extendedUrl)
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK:- NetworkRouter Method
    // , arrPar arrBodyParameter:[Any]? = nil
    func data_request(bodyencodedPata:[String:Any]? = nil, urlEncodingPara:[String:Any]? = nil, handler:@escaping handler ) {
        // used when using singleton class such as refresh token. to avoid multiple hit.
        guard task?.state != .running else {
            return
        }
        
        // check internet connection error

        if !isInternetAvailable() {
            handler(nil,NetworkError.networkNotAvailable,tag.extendedUrl,0)
            return
        }
        
        // when it is call for the first time, after refresh it will hit again
        if self.handlers == nil {
            handlers = handler
        }
        
        var urlString =  baseUrl  + tag.extendedUrl
        if let urlEncding = URLParameterEncoder.stringFromHttpParameters(data: urlEncodingPara) {
            urlString = urlString + "?" + urlEncding
        }
        print(urlString)
        if let url:URL = URL(string:urlString) {
            let session = URLSession.shared
            var request = URLRequest(url: url)
            request.httpMethod = tag.methodType.rawString
            request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            task = session.dataTask(with: request as URLRequest) {
                (data, response, error) in
                self.responseManagement(data: data, response: response, error: error)
            }
            task?.resume()
        } else {
            handler(nil,NetworkError.missingURL,tag.extendedUrl,0)
        }
    }
    
    func cancelTask() {
        task?.cancel()
    }

    func isRunning() -> Bool {
        switch task?.state {
        case .running?:
            return true
        default:
            return false
        }
    }
    
    private func responseManagement(data:Data?, response:URLResponse?, error:Error?) {
        let httpResponse = response as? HTTPURLResponse
        guard let data = data as Data?, let _:URLResponse = response, error == nil else {
            print("error")
            handlers?(nil,error,self.tag.extendedUrl,httpResponse?.statusCode)
            return
        }
        do {
            let parseJson = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments)
            print(parseJson)
            if let json = parseJson as? [String:Any] {
                if let errData = json["errors"] as? [String] , errData.count > 0 {
                    reponseType(nil,NetworkError.serverGenerated(message: errData[0], code: 0),self.tag.extendedUrl,httpResponse?.statusCode)
                } else {
                    reponseType(json,nil,self.tag.extendedUrl,httpResponse?.statusCode)
                }
            } else {
                reponseType(parseJson,nil,self.tag.extendedUrl,httpResponse?.statusCode)
            }
        }catch {
            print(error)
            let dataString = String(data: data, encoding: String.Encoding.utf8)
            print (dataString)
            reponseType(nil,error,self.tag.extendedUrl,httpResponse?.statusCode)
        }
    }
    
    private func reponseType(_ response:Any? , _ error:Error?,_ tag:String? , _ statusCode:Int?) {
        handlers?(response, error, tag,statusCode)
    }
}
