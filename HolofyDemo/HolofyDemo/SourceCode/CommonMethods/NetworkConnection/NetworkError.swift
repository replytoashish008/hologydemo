//
//  NetworkError.swift
//  HolofyDemo
//
//  Created by Ashish on 02/07/21.
//

import Foundation

// MARK:- NetworkError

enum NetworkError : Error , NetworkErrorProtocol {
    typealias RawValue = String
    
    case serverGenerated(message:String?,code:Int?)
    case parametersNil
    case encodingFailed
    case missingURL
    case networkNotAvailable
    case sessionExpired
    case userCancelled
    case somthingWentWrong
    
    case authenticationError
    case badRequest
    case outdated
    case failed
    case noData
    case unableToDecode
    
    func errorMessage() -> RawValue? {
        switch self {
        case .serverGenerated(message:let message, code:_):
            return message
        case .parametersNil:
            return ConstantNetworkLocalizeString.Parameters_were_nil
        case .encodingFailed:
            return ConstantNetworkLocalizeString.Parameter_encoding_failed
        case .missingURL:
            return ConstantNetworkLocalizeString.URL_is_nil
        case .networkNotAvailable:
            return ConstantNetworkLocalizeString.Please_check_your_internet_connection
        case .sessionExpired:
            return ConstantNetworkLocalizeString.Session_Expired
        case .userCancelled:
            return ""
        case .somthingWentWrong:
            return ConstantNetworkLocalizeString.Something_went_wrong
        case .authenticationError:
            return ConstantNetworkLocalizeString.You_need_to_be_authenticated_first
        case .badRequest:
            return ConstantNetworkLocalizeString.Bad_request
        case .outdated:
            return ConstantNetworkLocalizeString.The_url_you_requested_is_outdated
        case .failed:
            return ConstantNetworkLocalizeString.Network_request_failed
        case .noData:
            return ConstantNetworkLocalizeString.Response_returned_with_no_date_to_decode
        case .unableToDecode:
            return ConstantNetworkLocalizeString.We_could_not_decode_the_response
        }
    }
}
