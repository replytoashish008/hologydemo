//
//  ConstantNetworkLocalizeString.swift
//  HolofyDemo
//
//  Created by Ashish on 02/07/21.
//

import Foundation

struct ConstantNetworkLocalizeString {
    static let  Server_error = "Server error"
    static let  Parameters_were_nil = "Parameters were nil"
    static let  Parameter_encoding_failed = "Parameter encoding failed"
    static let  URL_is_nil = "URL is nil"
    static let  You_need_to_be_authenticated_first = "You need to be authenticated first"
    static let  Bad_request = "Bad request"
    static let  The_url_you_requested_is_outdated = "The url you requested is outdated"
    static let  Network_request_failed = "Network request failed"
    static let  Response_returned_with_no_date_to_decode = "Response returned with no date to decode"
    static let  We_could_not_decode_the_response = "We could not decode the response"
    static let  Please_check_your_internet_connection = "Please check your internet connection"
    static let  Session_Expired = "Session Expired"
    static let  Something_went_wrong = "Something went wrong"
    static let  Cancel = "Cancel"
    static let  Retry = "Retry"
}

