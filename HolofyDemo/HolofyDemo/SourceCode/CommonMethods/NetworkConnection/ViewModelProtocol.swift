//
//  ViewModelProtocol.swift
//  HolofyDemo
//
//  Created by Ashish on 02/07/21.
//

import Foundation

@objc protocol ViewModelProtocol {
    @objc optional func validation() -> String?
    func apiHit(handler:@escaping (_ errorMessage:String?)->Void)
}

extension ViewModelProtocol {
    
    func getErrorMessage(error:Error?) -> String? {
        if let error = error {
            if let error = error as? NetworkError {
                return error.errorMessage()
            } else {
                if (error as NSError).code == NSURLErrorCancelled {
                    return nil
                }
                return error.localizedDescription
            }
        }
        return nil
    }
    
    func getErrorMessage(error:Error?,response:Any?,statusCode:Int?) -> String? {
        if let error = error {
            return getErrorMessage(error: error)
        } else {
            if let response = response as? [String:Any] {
                if let code = response["code"] as? Int {
                    if code == 200 {
                        return nil
                    }
                }
                if let statusCode = statusCode, statusCode >= 200 && statusCode <= 299 {
                    return nil
                } else if let message = response["message"] as? String {
                    return message
                } else if let message = response["Message"] as? String {
                    return message
                } else {
                    return "Something went wrong"
                }
            }  else if let statusCode = statusCode, statusCode >= 200 && statusCode <= 299 {
                return nil
            }  else {
                return "Something went wrong"
            }
        }
    }
}

