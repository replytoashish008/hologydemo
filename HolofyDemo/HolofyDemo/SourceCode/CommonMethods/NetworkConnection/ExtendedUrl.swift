//
//  ExtendedUrl.swift
//  HolofyDemo
//
//  Created by Ashish on 02/07/21.
//

import Foundation

// MARK:- ExtendedUrl
enum ExtendedUrl: NetworkUrl {
    case weather

    var methodType:APIHttpMethod {
        return .get
    }
    var extendedUrl:String {
        switch self {
        case .weather:
            return "weather"
        }
    }
    var apiContentType: APIContentType {
        return .x_www_form_urlencoded
    }
}

