//
//  StringExt.swift
//  HolofyDemo
//
//  Created by Ashish on 02/07/21.
//

import Foundation

extension String {
    
    var trimString:String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
}
