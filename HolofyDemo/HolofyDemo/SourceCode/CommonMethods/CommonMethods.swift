//
//  CommonMethods.swift
//  HolofyDemo
//
//  Created by Ashish on 26/06/21.
//

import UIKit

//MARK:- Alert
func showAlertMessage(string:String? , viewController : UIViewController?){
    if let string = string {
        let alert = UIAlertController(title: "", message: string, preferredStyle: .alert)
        let dismiss = UIAlertAction(title: "Dismiss", style: .default , handler: nil)
        alert.addAction(dismiss)
        
        DispatchQueue.main.async {
            if let viewController = viewController {
                viewController.present(alert, animated: true, completion: nil)
            } else if let viewController = UIApplication.shared.keyWindow?.rootViewController {
                viewController.present(alert, animated: true, completion: nil)
            }
        }
    }
}


//MARK:- ActivityIndicator
func createActivityIndicator(view:UIView? ) -> UIView?{
    
    if let view = view {
        let actityIndc = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        actityIndc.backgroundColor = UIColor.black
        actityIndc.alpha = 0.8
        actityIndc.hidesWhenStopped = true
        actityIndc.layer.cornerRadius  = 10
        actityIndc.startAnimating()
        
        let activityView = UIView()
        //    if disableTouch == true {
        var frame = view.frame
        frame.origin.y = 0
        activityView.frame = frame
        actityIndc.frame = CGRect.init(x: view.frame.size.width/2 - 45, y: view.frame.size.height/2, width: 90, height: 90)
        
        activityView.backgroundColor = UIColor.clear
        activityView.addSubview(actityIndc)
        view.addSubview(activityView)
        
        return activityView
    }
    return nil
}

func removeActivityIndicatorFromSuperView(activity:UIView?){
    if let activity = activity{
        DispatchQueue.main.async {
            activity.removeFromSuperview()
        }
    }
}

//MARK:- Toast

func showToast(message:String?) {
    
    if let windows = UIApplication.shared.delegate?.window {
        let label = UILabel(frame: CGRect.zero)
        label.textAlignment = NSTextAlignment.center
        label.text = message
        label.font = UIFont.systemFont(ofSize: 13)
        label.adjustsFontSizeToFitWidth = true
        
        label.backgroundColor =  UIColor.lightGray //UIColor.whiteColor()
        label.textColor = UIColor.white //TEXT COLOR
        
        label.sizeToFit()
        label.numberOfLines = 4
        label.layer.shadowColor = UIColor.gray.cgColor
        label.layer.shadowOffset = CGSize(width:4,height: 3)
        label.layer.shadowOpacity = 0.3
        label.frame = CGRect(x:320,y: 64,width: windows?.frame.size.width ?? 240, height:44)
        label.alpha = 1
        
        windows?.addSubview(label)
        
        var basketTopFrame: CGRect = label.frame;
        basketTopFrame.origin.x = 0;
        
        UIView.animate(withDuration: 2.0, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.1, options: UIView.AnimationOptions.curveEaseOut, animations: { () -> Void in
            label.frame = basketTopFrame
        },  completion: {
            (value: Bool) in
            UIView.animate(withDuration: 2.0, delay: 2.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.1, options: UIView.AnimationOptions.curveEaseIn, animations: { () -> Void in
                label.alpha = 0
            },  completion: {
                (value: Bool) in
                label.removeFromSuperview()
            })
        })
    }
    
}

