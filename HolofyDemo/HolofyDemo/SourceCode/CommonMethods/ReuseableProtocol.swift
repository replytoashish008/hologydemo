//
//  ReuseableProtocol.swift
//  HolofyDemo
//
//  Created by Ashish on 26/06/21.
//

import Foundation

import UIKit

protocol ReuseableProtocol {
    static var reusableIdentifier:String {
        get
    }
}

extension ReuseableProtocol {
    static var reusableIdentifier:String {
        get {
            return "\(self)"
        }
    }
}
