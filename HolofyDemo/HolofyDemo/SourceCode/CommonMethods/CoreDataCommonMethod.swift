//
//  CoreDataCommonMethod.swift
//  HolofyDemo
//
//  Created by Ashish on 26/06/21.
//

import UIKit
import CoreData

//MARK:- ManagedObjectContext
var context = getManagedObjectContext()
func getManagedObjectContext() -> NSManagedObjectContext  {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    return appDelegate.persistentContainer.viewContext
}

//MARK:- Fetch ResultController

func getFetchResultController(sortKey:String,ascending:Bool,predicate:NSPredicate?,classObject:String) -> NSFetchedResultsController<NSFetchRequestResult> {

    let fetchRequest = getfetchRequest(sortKey: sortKey, ascending: true, predicate: predicate, classObject: classObject)

    // Create Fetched Results Controller
    let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)

    // Configure Fetched Results Controller
    do {
        try fetchedResultsController.performFetch()
    } catch {

        let fetchError = error as NSError
        print("Unable to Perform Fetch Request")
        print("\(fetchError), \(fetchError.localizedDescription)")

    }

    return fetchedResultsController
}

//MARK:- Fetch Request
func getFetchRecord(predicate:NSPredicate?,classObject:String) -> [NSManagedObject] {
    let fetchRequest = getfetchRequest(sortKey: nil, ascending: true, predicate: predicate, classObject: classObject)

    // Helpers
    var result = [NSManagedObject]()

    do {
        // Execute Fetch Request
        let records = try context.fetch(fetchRequest)
        if let records = records as? [NSManagedObject] {
            result = records
        }
    } catch {
        print("Unable to fetch managed objects for entity \(classObject).")
    }

    return result
}

func getfetchRequest(sortKey:String?,ascending:Bool,predicate:NSPredicate?,classObject:String) -> NSFetchRequest<NSFetchRequestResult> {
    let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: classObject)
    // Configure Fetch Request
    if let sortKey = sortKey {
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: sortKey, ascending: ascending)]
    }
    if let predicate = predicate {
        fetchRequest.predicate =  predicate
    }
    return fetchRequest
}

//MARK:- Save
func saveCoreData() {
    context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
    do {
        try context.save()
    } catch {
        print ("There is an error in deleting records")
    }
}



//MARK:- delete
func deleteRequest(deleteRequest: NSBatchDeleteRequest) {
    do {
        try context.execute(deleteRequest)
    } catch {
        print ("There is an error in deleting records")
    }
}

func deleteAllData() {
    deleteRequest(deleteRequest: NSBatchDeleteRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>(entityName: Weather.reusableIdentifier)))
}

