//
//  WeatherDetailView.swift
//  HolofyDemo
//
//  Created by Ashish on 26/06/21.
//

import UIKit
import CoreData

class WeatherDetailView: UIView {

    // MARK:- IBOutlet
    @IBOutlet weak var lblDate:UILabel!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblCurrentTemp:UILabel!
    @IBOutlet weak var lblMiniTemp:UILabel!
    @IBOutlet weak var lblMaxTemp:UILabel!
    @IBOutlet weak var lblCoordinate:UILabel!
    @IBOutlet weak var lblLocation:UILabel!
    @IBOutlet weak var lblWeatherDesc:UILabel!
    
    // MARK:- Methods
    func updateView(with location: Location) {
        lblTitle.text = location.title
        if let marker = location.hasMarker {
            lblCoordinate.text = "\(marker.lat)" + ", " + "\(marker.longi)"
            lblLocation.text = marker.address
        }
        
        if let weatherDetail = location.hasMarker?.hasWeather {
            lblDate.text = "\(getStringDateFromDate(date: weatherDetail.date, requiredDateFormate: .longDate) ?? "")"
            lblCurrentTemp.text = "\(weatherDetail.currentWeather)"
            lblMaxTemp.text = "\(weatherDetail.maxTemp)"
            lblMiniTemp.text = "\(weatherDetail.minTemp)"
            lblWeatherDesc.text = weatherDetail.weatherDesc
        } else {
            lblDate.text = "--"
            lblCurrentTemp.text = "--"
            lblMaxTemp.text = "--"
            lblMiniTemp.text = "--"
            lblWeatherDesc.text = "--"
        }
    }
}
