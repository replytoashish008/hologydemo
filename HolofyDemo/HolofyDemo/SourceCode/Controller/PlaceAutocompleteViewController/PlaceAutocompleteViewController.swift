//
//  FavouriteVC.swift
//  HolofyDemo
//
//  Created by Ashish on 26/06/21.
//

import GooglePlaces
import UIKit
import MapKit

class PlaceAutocompleteViewController: UIViewController {

    //MARK:- IBOutlet
    @IBOutlet weak var tableVIew: UITableView!
    @IBOutlet weak var srchBar: UISearchBar!

    //MARK:- Private
    private let viewmodel = PlaceAutocompleteViewModel()

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewmodel.autoCompletDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewmodel.fetchResultForFav()
        tableVIew.reloadData()
    }
    
    //MARK:- Privte Methods
    private func resetView() {
        viewmodel.reset()
        self.srchBar.text = ""
        self.tableVIew.reloadData()
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            switch identifier {
            case PlaceAutocompleteViewModel.Segue.detail:
                if let destination = segue.destination as? DetailPageVC {
                    if let location = sender as? Location {
                        destination.set(location: location)
                    } else if let localSearch = sender as? MKLocalSearchCompletion {
                        destination.set(localSearch: localSearch)
                    }
                }
            default:
                break
            }
        }
    }
}
extension PlaceAutocompleteViewController:UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        viewmodel.searchTextWtih(text: searchBar.text)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        resetView()
    }
        
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewmodel.searchTextWtih(text: searchText)
    }

}

//MARK:- UITableViewDataSource
extension PlaceAutocompleteViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  viewmodel.getRow()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //I've created SearchCell beforehand; it might be your cell type
        let cell = self.tableVIew.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath)
        cell.textLabel?.attributedText = viewmodel.getTitle(at: indexPath)
        cell.accessoryView = UIImageView(image:viewmodel.getImage(at: indexPath))
        return cell
    }
    
}

//MARK:- UITableViewDataSource
extension PlaceAutocompleteViewController:UITableViewDataSource {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let weather = viewmodel.getSearchAt(indexPath: indexPath)
        if viewmodel.isActive {
            self.performSegue(withIdentifier: PlaceAutocompleteViewModel.Segue.detail, sender: weather)
        } else {
            self.performSegue(withIdentifier: PlaceAutocompleteViewModel.Segue.detail, sender: viewmodel.getFavAt(indexPath:indexPath))
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return viewmodel.getHeaderHeight()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if !viewmodel.isActive {
            return tableView.dequeueReusableCell(withIdentifier: "HeaderSection")
        }
        return nil
    }
}

//MARK:- PlaceAutocompleteProtocol
extension PlaceAutocompleteViewController: PlaceAutocompleteProtocol {
    func searchLocationResultFound() {
        DispatchQueue.main.async {
            self.tableVIew.reloadData()
        }
    }
    
    func searchLocationDidFailWith(error: Error) {
        showAlertMessage(string: error.localizedDescription, viewController: nil)
    }
}
