//
//  FavouriteViewModel.swift
//  HolofyDemo
//
//  Created by Ashish on 26/06/21.
//

import Foundation
import MapKit

protocol PlaceAutocompleteProtocol :AnyObject {
    func searchLocationResultFound()
    func searchLocationDidFailWith(error:Error)
}

class PlaceAutocompleteViewModel:NSObject {
    
    struct Segue {
        static let detail = "DetailSegue"
    }
    
    private var favWeathers = [Location]()
    private var filteredTableData = [MKLocalSearchCompletion]()
    private var searchText:String = ""
    weak var autoCompletDelegate:PlaceAutocompleteProtocol?
    
    //create a completer
    private lazy var searchCompleter: MKLocalSearchCompleter = {
        let localSearchCompleter = MKLocalSearchCompleter()
        localSearchCompleter.filterType = .locationsOnly
        localSearchCompleter.delegate = self
        localSearchCompleter.resultTypes = .address
        return localSearchCompleter
    }()
    
    var isActive:Bool {
        return searchText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count > 0
    }
    
    func searchTextWtih(text:String?) {
        searchText = text ?? ""
        if let searchText = text , !searchText.isEmpty {
            searchCompleter.queryFragment = searchText
        }
    }
    
    func getRow() -> Int {
        if isActive {
            return filteredTableData.count
        } else {
            return favWeathers.count
        }
    }
    
    func getFavAt(indexPath:IndexPath) -> Location? {
        if indexPath.row < favWeathers.count {
            return favWeathers[indexPath.row]
        }
        return nil
    }
    
    func getSearchAt(indexPath:IndexPath) -> MKLocalSearchCompletion? {
        if indexPath.row < filteredTableData.count {
            return filteredTableData[indexPath.row]
        }
        return nil
    }
    
    func getTitle(at indexPath:IndexPath ) -> NSAttributedString {
        if isActive {
            if let searchResult = getSearchAt(indexPath: indexPath) {
                let attriString = NSMutableAttributedString.init(string: searchResult.title)
                if let ranges = searchResult.titleHighlightRanges as? [NSRange] {
                    for range in ranges {
                        attriString.addAttributes([NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 18)], range:range)
                    }
                }
                return attriString
            }
            return NSAttributedString.init(string: "")
        } else {
            return NSAttributedString.init(string: getFavAt(indexPath: indexPath)?.title ?? "")
        }
    }
    
    func getHeaderTitle(at section:Int = 0 ) -> String? {
        if isActive {
            return nil
        } else {
            return "Favourite Places"
        }
    }
    
    func getHeaderHeight(at section:Int = 0 ) -> CGFloat {
        if isActive {
            return 0.0
        } else {
            return 56.0
        }
    }
    
    func getImage(at indexPath:IndexPath) -> UIImage? {
        if isActive {
            return nil
        } else {
            return UIImage.init(named: "star")
        }
    }
    
    func reset() {
        searchText = ""
        filteredTableData = []
    }
    
    func fetchResultForFav() {
        favWeathers = Location.getAllFav()
    }
    
    func removeFav(at indexPath:IndexPath) {
        if indexPath.row < favWeathers.count {
            favWeathers.remove(at: indexPath.row)
        }
    }
}

// MARK:- MKLocalSearchCompleterDelegate
extension PlaceAutocompleteViewModel:  MKLocalSearchCompleterDelegate {
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        //get result, transform it to our needs and fill our dataSource
        self.filteredTableData = completer.results
        autoCompletDelegate?.searchLocationResultFound()
    }

    func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
        //handle the error
        print(error.localizedDescription)
        autoCompletDelegate?.searchLocationDidFailWith(error: error)
    }
}
