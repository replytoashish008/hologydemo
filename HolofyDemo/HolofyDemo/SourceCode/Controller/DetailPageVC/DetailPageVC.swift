//
//  DetailPageVC.swift
//  HolofyDemo
//
//  Created by Ashish on 26/06/21.
//

import Foundation
import MapKit

class DetailPageVC: UIViewController {

    // MARK:- IBOutlet
    @IBOutlet weak var mapVew: MKMapView!
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var weatherView: WeatherDetailView!
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    
    // MARK: - Properties
    var activityIndicator: UIView?
    let viewModel = DetailPageViewModel()
    
    // MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        apiHit()
        updateFavButton()
        // Do any additional setup after loading the view.
    }
    
    // MARK:- Methods
    func set(location:Location) {
        viewModel.object = location
    }

    func set(localSearch:MKLocalSearchCompletion) {
        viewModel.localSearch = localSearch
    }
    
    // MARK:- Methodsal

    @IBAction func btnClickOnFav() {
        viewModel.object?.updateDataFav(isFav: !(viewModel.object?.isFav ?? true))
        updateFavButton()
    }
    
    // MARK:- Methodsal

    private func updateView() {
        setAnnotation()
        loadWeatherView()
        updateFavButton()
    }
    
    private func updateFavButton() {
        btnFav.isSelected = viewModel.object?.isFav ?? false
    }
    
    private func setAnnotation() {
        if let annotation = self.viewModel.getAnnotation() {
            self.mapVew.addAnnotation(annotation)
        }
        setRegion()
    }
    
    private func setRegion() {
        if let region = self.viewModel.getRegion() {
            self.mapVew.setRegion(region, animated: false)
        }
    }
    
    // MARK: - Private Methods
    private func apiHit() {
        activityIndicator = createActivityIndicator(view: self.view)
        viewModel.apiHit {[weak self] (error) in
            if let errorMessage = error {
                DispatchQueue.main.async {
                    showToast(message: errorMessage)
                }
            }
            
            removeActivityIndicatorFromSuperView(activity: self?.activityIndicator)
            DispatchQueue.main.async {
                self?.updateView()
            }
        }
    }
    
    private func loadWeatherView() {
        if let object = viewModel.object {
            visualEffectView.isHidden = false
            weatherView.isHidden = false
            weatherView.updateView(with: object )
        }
    }
}
