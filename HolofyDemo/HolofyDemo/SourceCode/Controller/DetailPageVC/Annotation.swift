//
//  Annotation.swift
//  HolofyDemo
//
//  Created by Ashish on 26/06/21.
//

import Foundation

import CoreLocation
import MapKit

class Annotation:NSObject, MKAnnotation {
    
    init?(coordinate:CLLocationCoordinate2D?) {
        if let coordinate = coordinate {
            self.coordinate = coordinate
        } else {
            return nil
        }
    }
    
    var coordinate: CLLocationCoordinate2D
    var title:String?
}
