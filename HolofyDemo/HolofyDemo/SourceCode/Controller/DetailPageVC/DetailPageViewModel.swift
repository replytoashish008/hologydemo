//
//  DetailPageViewModel.swift
//  HolofyDemo
//
//  Created by Ashish on 26/06/21.
//

import Foundation
import MapKit

class DetailPageViewModel: NSObject {
    
    struct Keys {
        static let latitude = "lat"
        static let longitude = "lon"
        static let appid = "appid"
        static let cod = "cod"
        static let message = "message"
        static let units = "units"
        static let metric = "metric"
    }
    
    // MARK:- typealias
    typealias success = () -> Void
    typealias error = (_ error:String?) -> Void
    
    // MARK:- Constant
    private var coordinate :CLLocationCoordinate2D? {
        get {
            if let lat = object?.hasMarker?.lat , let longi = object?.hasMarker?.longi  {
                return CLLocationCoordinate2D.init(latitude: lat, longitude: longi)
            }
            return nil
        }
    }
    var object:Location?
    var localSearch:MKLocalSearchCompletion?

    // MARK:- Methods
    
    func getAnnotation() -> MKAnnotation? {
        if let annotation = Annotation(coordinate: coordinate) {
            return annotation
        }
        return nil
    }
    
    func getRegion() -> MKCoordinateRegion? {
        if let coordinate = coordinate {
            let span = MKCoordinateSpan(latitudeDelta: 0.0275, longitudeDelta: 0.0275)
            return MKCoordinateRegion.init(center: coordinate, span: span)
        }
        return nil
    }
}

extension DetailPageViewModel:ViewModelProtocol {
    
    private func getDetail(localSearch: MKLocalSearchCompletion, completion:@escaping (_ error:Error?)->Void) {
        let searchRequest = MKLocalSearch.Request.init(completion: localSearch)
        let search = MKLocalSearch(request: searchRequest)
        search.start { [weak self] (response, error) in
            if let error = error {
                completion( error)
            } else if let mapItems = response?.mapItems , mapItems.count > 0 {
                let placemark = mapItems[0].placemark
                self?.object?.updateDataMarker(data: mapItems[0].placemark)
                completion( nil)
            } else {
                completion(NetworkError.somthingWentWrong)
            }
        }
    }
    
    func apiHit(handler: @escaping (String?) -> Void) {
        if let _  = object {
            self.getWeatherAPIDetails(handler: { errorStr in
                handler(errorStr)
            })
        } else if let localSearch = localSearch {
            object = Location.saveData(title: localSearch.title)
            if let _ = object?.hasMarker {
                self.getWeatherAPIDetails(handler: { errorStr in
                    handler(errorStr)
                })
            } else {
                getDetail(localSearch: localSearch) { [weak self] (error) in
                    if let error = error {
                        print(error.localizedDescription)
                        handler(error.localizedDescription)
                    } else {
                        self?.getWeatherAPIDetails(handler: { errorStr in
                            handler(errorStr)
                        })
                    }
                }
            }
        }
    }
    
    func getWeatherAPIDetails(handler: @escaping (String?) -> Void) {
        if let coordinate = coordinate {
            NetworkOperations.init(tag: ExtendedUrl.weather).data_request( urlEncodingPara: [Keys.latitude:coordinate.latitude,Keys.longitude:coordinate.longitude, Keys.appid:Constants.APIKey]) { [weak self] (response, error, tag, statusCode) in
                if let error = self?.getErrorMessage(error: error, response: response, statusCode: statusCode) {
                    handler(error)
                } else {
                    self?.object?.updateDataWithWeather(data: response as? [String : Any])
                    handler(nil)
                }
            }
        } else {
            handler("Coordinate not found")
        }
    }
}
